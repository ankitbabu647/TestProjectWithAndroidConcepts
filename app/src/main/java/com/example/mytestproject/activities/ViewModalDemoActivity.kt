package com.example.mytestproject.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.mytestproject.R
import com.example.mytestproject.databinding.ActivityViewModalDemoBinding
import com.example.mytestproject.modals.ViewModalCountviewModal

class ViewModalDemoActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityViewModalDemoBinding
    private lateinit var viewmodal:ViewModalCountviewModal
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_view_modal_demo)
        binding.btnCount.setOnClickListener(this)
        viewmodal= ViewModelProvider(this)[ViewModalCountviewModal::class.java]
            binding.tvDisplay.text=viewmodal.i.toString()

        
    }

    override fun onClick(v: View?) {
     viewmodal.setUpdateValue()
        binding.tvDisplay.text=viewmodal.i.toString()


    }
}